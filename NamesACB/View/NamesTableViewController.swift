//
//  ViewController.swift
//  NamesACB
//
//  Created by Anna Beltrami on 11/09/2019.
//  Copyright © 2019 anna. All rights reserved.
//
import CoreData
import UIKit

class NamesTableViewController: UIViewController {
    

    //MARK: Properties
    var names: [NSManagedObject]? {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    var filtered: Bool = true {
        didSet {
            fetchNames()
        }
    }
    
    //MARK: UIProperties
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var showAllButton: UIBarButtonItem!
    
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Names"
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchNames()
    }
    
    @IBAction func showAll(_ sender: Any) {
        filtered.toggle()
    }
    
    
    func fetchNames() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Person")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        do {
            let unfiltered = try context.fetch(fetchRequest)
            if filtered == true {
                names = unfiltered.filter { (person) -> Bool in
                    guard let fullName: String = person.value(forKeyPath: "name") as? String else { return false }
                    let names = fullName.split(separator: " ")
                    for n in names {
                        if !n.first!.isUppercase{
                            return false
                        }
                    }
                    return true
                }
            } else {
                names = unfiltered
            }
        } catch let error as NSError {
            print("Error \(error.localizedDescription)")
        }
    }
    
    
}

// MARK: - UITableViewDataSource
extension NamesTableViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = names?.count else { return 0 }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = names?[indexPath.row].value(forKeyPath: "name") as? String
        cell.backgroundColor = .darkGray
        cell.textLabel?.textColor = .white
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        delete(object: (names?[indexPath.row])!)
        names?.remove(at: indexPath.row)
        self.tableView.reloadData()
    }
    
}

protocol DeleteProtocol: class {
    func delete(object: NSManagedObject)
}

extension NamesTableViewController: DeleteProtocol {
    
    func delete(object: NSManagedObject) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        managedContext.delete(object)
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Error \(error.localizedDescription)")
        }
        
    }
    
}

