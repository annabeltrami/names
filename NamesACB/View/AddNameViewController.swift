//
//  AddNameViewController.swift
//  NamesACB
//
//  Created by Anna Beltrami on 12/09/2019.
//  Copyright © 2019 anna. All rights reserved.
//

import CoreData
import UIKit

class AddNameViewController: UIViewController {

    //MARK: Properties
    
    
    //MARK: UI Properties
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    
    //MARK: IBACTIONS
    @IBAction func saveName(_ sender: Any) {
        save(name: nameTextField.text!)
        nameTextField.text = ""
        saveButton.disable()
        
    }
    
    //MARK: Lifecycle
  
    override func viewDidLoad() {
        super.viewDidLoad()
        saveButton.disable()
        title = "Add New Name"
        nameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
    }

    
    @objc func textFieldDidChange(_ textField: UITextField) {
        guard let name = nameTextField.text else { return }
        if name.isEmpty {
            saveButton.disable()
        } else {
            saveButton.enable()
        }
    }
}

protocol AddNameProtocol: class {
    func save(name: String)
}

extension AddNameViewController: AddNameProtocol {
   
    func save(name: String){
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }

        let managedContext = appDelegate.persistentContainer.viewContext

        let entity = NSEntityDescription.entity(forEntityName: "Person", in: managedContext)!
        let person = NSManagedObject(entity: entity, insertInto: managedContext)
        person.setValue(name, forKeyPath: "name")

        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Error \(error.localizedDescription)")
        }
        
    }
    
}

extension UIButton {
  
    func enable() {
        isEnabled = true
        setTitleColor(Color.accent, for: .normal)
    }
    
    func disable() {
        isEnabled = false
        setTitleColor(UIColor.lightGray, for: .normal)
    }
    
}
